<?php

the_post();
get_header();
$fields = get_fields();
$post_link = get_the_permalink();
$f_title = opt('post_form_title');
$f_subtitle = opt('post_form_subtitle');
?>

<article class="page-body">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col">
				<h1 class="page-title text-center mb-4"><?php the_title(); ?></h1>
			</div>
		</div>
		<div class="row justify-content-between">
			<div class="col-lg-7 col-12 post-content-col">
				<div class="base-output">
					<?php the_content(); ?>
				</div>
				<div class="socials-share">
					<span class="share-text"><?= esc_html__('שתף', 'leos'); ?></span>
					<!--	WHATSAPP-->
					<a href="https://api.whatsapp.com/send?text=<?php the_title(); echo $post_link; ?>" class="social-share-link">
						<img src="<?= ICONS ?>share-whatsapp.png" alt="whatsapp">
					</a>
					<a href="https://www.facebook.com/sharer/sharer.php?u=#<?= $post_link; ?>" target="_blank"
					   class="social-share-link">
						<img src="<?= ICONS ?>share-facebook.png" alt="facebook">
					</a>
					<!--	MAIL-->
					<a href="mailto:?subject=&body=<?= $post_link; ?>" target="_blank"
					   class="social-share-link">
						<img src="<?= ICONS ?>share-mail.png" alt="mail">
					</a>
				</div>
				<?php if ($fields['post_file']) : ?>
					<a class="share-item-file" download href="<?= $fields['post_file']['url']; ?>">
						<span class="share-text">
							<?= (isset($fields['post_file']['title']) && $fields['post_file']['title']) ? $fields['post_file']['title'] : 'הורידו את הקובץ'?>
						</span>
						<span class="share-item">
							<img src="<?= ICONS ?>pdf.png" alt="download-file">
						</span>
					</a>
				<?php endif; ?>
			</div>
			<div class="col-lg-5 col-12 position-relative">
				<div class="post-form-col">
					<?php if ($f_title && $f_subtitle) : ?>
						<div class="d-flex flex-column align-items-center mb-3">
							<h2 class="form-title"><?= $f_title; ?></h2>
							<h2 class="form-subtitle"><?= $f_subtitle; ?></h2>
						</div>
					<?php endif;
					getForm('57'); ?>
				</div>
			</div>
		</div>
	</div>
</article>
<?php
$postId = get_the_ID();
$post_terms = wp_get_object_terms($postId, 'category', ['fields' => 'ids']);
$samePosts = [];
$samePosts = get_posts([
	'posts_per_page' => 4,
	'post_type' => 'post',
	'post__not_in' => array($postId),
	'tax_query' => [
		[
			'taxonomy' => 'category',
			'field' => 'term_id',
			'terms' => $post_terms,
		],
	],
]);
if ($samePosts === NULL) {
	$samePosts = get_posts([
		'posts_per_page' => 4,
		'orderby' => 'rand',
		'post_type' => 'post',
		'post__not_in' => array($postId),
	]);
}
if ($samePosts) : ?>
	<section class="posts-block mt-5">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col">
					<h2 class="block-title text-center mb-4">
						<?= $fields['same_title'] ? $fields['same_title'] : esc_html__('נושאים נוספים בתחום', 'leos');?>
					</h2>
				</div>
			</div>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($samePosts as $post) {
					get_template_part('views/partials/card', 'post',
						[
							'post' => $post,
						]);
				} ?>
			</div>
		</div>
	</section>
<?php
endif;
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
get_footer(); ?>
