<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );
get_template_part('views/partials/repeat', 'breadcrumbs');
?>
	<div class="container mt-4">
		<div class="row">
			<div class="col-12">
				<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
					<h1 class="woocommerce-products-header__title page-title text-center">
						<?php woocommerce_page_title(); ?>
					</h1>
				<?php endif; ?>
				<div class="base-output text-center mb-4">

					<?php
					/**
					 * Hook: woocommerce_archive_description.
					 *
					 * @hooked woocommerce_taxonomy_archive_description - 10
					 * @hooked woocommerce_product_archive_description - 10
					 */
					do_action( 'woocommerce_archive_description' );

					?>
				</div>
				<div class="home-products" id="home-shop">
					<div class="row justify-content-center">
						<div class="col-12">
							<?php if (woocommerce_product_loop()): ?>
								<div class="row justify-content-center">
									<?php if ( wc_get_loop_prop( 'total' ) )  {
										$x = 0;
										while ( have_posts() ) {
											the_post();
											/**
											 * Hook: woocommerce_shop_loop.
											 */
											do_action( 'woocommerce_shop_loop' );
											echo '<div class="col-lg-4 col-sm-6 col-12 product-col">';
											get_template_part('views/partials/card', 'product',
													[
															'product' => get_the_ID(),
													]);
											echo '</div>';
											$x++;
										}
									}
									wp_reset_postdata();
									?>
									<div class="col-12 d-flex justify-content-end mt-3 mb-5">
										<?php do_action( 'woocommerce_after_shop_loop' ); ?>
									</div>
								</div>
							<?php else: ?>
								<?php

								/**
								 * Hook: woocommerce_no_products_found.
								 *
								 * @hooked wc_no_products_found - 10
								 */
								do_action( 'woocommerce_no_products_found' );
								?>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php get_footer( 'shop' );
