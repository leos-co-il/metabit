<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="icon" href="<?= IMG ?>favicon.ico" type="image/x-icon">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php wp_head(); ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body <?php body_class(ENV); ?>>

<?php if(ENV === 'dev'): ?>
    <script>
        const timerStart = Date.now();
    </script>
<div class="debug bg-danger border">
    <p class="width">
        <span>Width:</span>
        <span class="val"></span>
    </p>
    <p class="height">
        <span>Height:</span>
        <span class="val"></span>
    </p>
    <p class="media-query">
        <span>Media Query:</span>
        <span class="val"></span>
    </p>
    <p class="zoom">
        <span>Zoom:</span>
        <span class="val"></span>
    </p>
    <p class="dom-ready">
        <span>DOM Ready:</span>
        <span class="val"></span>
    </p>
    <p class="load-time">
        <span>Loading Time:</span>
        <span class="val"></span>
    </p>
</div>
<?php endif;
$tel = opt('tel');
$whatsapp = opt('whatsapp'); ?>
<header class="sticky">
    <div class="container-fluid">
		<div class="row align-items-center justify-content-between position-relative">
			<div class="drop-menu">
				<nav class="header-nav">
					<?php getMenu('header-menu', '1'); ?>
				</nav>
			</div>
			<div class="col-auto d-flex justify-content-start align-items-center">
				<button class="hamburger hamburger--spin menu-trigger" type="button">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
				</button>
				<div class="col-auto reg-col">
					<a href="<?= is_user_logged_in() ? wp_login_url() : wp_registration_url(); ?>" class="register-link">
						<?= svg_simple(ICONS.'user.svg'); ?>
					</a>
				</div>
				<div class="langs-wrap">
					<?php site_languages(); ?>
				</div>
			</div>
			<div class="col-auto d-flex justify-content-end align-items-center">
				<?php if ($tel || $whatsapp) : ?>
					<div class="tel-wrapper">
						<?php if ($tel) : ?>
							<a href="tel:<?= $tel; ?>" class="header-tel d-flex justify-content-center align-items-center">
								<img src="<?= ICONS ?>header-tel.png">
								<span class="tel-number"><?= $tel; ?></span>
							</a>
						<?php endif; if ($whatsapp) : ?>
							<div class="tel-delimetr"></div>
							<a href="https://api.whatsapp.com/send?phone=<?= $whatsapp; ?>" target="_blank" class="header-tel">
								<span class="tel-number"><?= $whatsapp; ?></span>
							</a>
						<?php endif; ?>
					</div>
				<?php endif;
				if ($logo = opt('logo')) : ?>
					<a href="<?= home_url(); ?>" class="logo">
						<img src="<?= $logo['url'] ?>" alt="logo">
					</a>
				<?php endif; ?>
			</div>
		</div>
	</div>
</header>

<div class="socials-fix">
	<?php if ($whatsapp) : ?>
		<a href="https://api.whatsapp.com/send?phone=<?= $whatsapp; ?>" target="_blank" class="social-link">
			<img src="<?= ICONS ?>foo-whatsapp.png" alt="call-whatsapp">
		</a>
	<?php endif; ?>
	<div class="pop-trigger social-link">
		<img src="<?= ICONS ?>foo-mail.png" alt="contact-us">
	</div>
</div>
<div class="pop-great"></div>
<div class="float-form">
	<div class="pop-form">
		<?php if ($popTitle = opt('pop_form_title')) : ?>
			<h2 class="form-title">
				<?= $popTitle; ?>
			</h2>
		<?php endif;
		if ($popText = opt('pop_form_subtitle')) : ?>
			<p class="form-subtitle">
				<?= $popText; ?>
			</p>
		<?php endif; ?>
		<?php getForm('55'); ?>
	</div>
</div>
<?php $link_1 = opt('fix_link_1');
$link_2 = opt('fix_link_2');
if ($link_1 || $link_2) : ?>
	<div class="fixed-links">
		<a class="fixed-link" href="<?= $link_1['url']; ?>">
			<?= (isset($link_1['title']) && $link_1['title']) ? $link_1['title'] : esc_html__('הצטרפו עכשיו', 'leos'); ?>
		</a>
		<a class="fixed-link" href="<?= $link_2['url']; ?>">
			<?= (isset($link_2['title']) && $link_2['title']) ? $link_2['title'] : esc_html__('בדיקת תוקף ביטוח', 'leos'); ?>
		</a>
	</div>
<?php endif; ?>
