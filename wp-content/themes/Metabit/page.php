<?php

the_post();
get_header();
$fields = get_fields();

?>
<article>
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container mb-5">
		<div class="row">
			<div class="col">
				<div class="base-output">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
</article>
<?php get_footer(); ?>
