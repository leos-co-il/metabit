<?php
/*
Template Name: צור קשר
*/

get_header();
$fields = get_fields();

$tel = opt('tel');
$mail = opt('mail');
$address = opt('address');
$map = opt('map_image');
$hours = opt('open_hours');
?>

<article class="page-body">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container mb-5">
		<div class="row justify-content-center">
			<div class="col-xl-8 col-lg-10 col-12">
				<h1 class="page-title mb-4">
					<?php the_title(); ?>
				</h1>
				<div class="base-output text-center">
					<?php the_content(); ?>
				</div>
				<div class="row">
					<?php if ($tel) : ?>
						<div class="col-lg-3 col-sm-6 contact-item contact-item-link wow fadeInUp"
							 data-wow-delay="0.2s">
							<div class="contact-icon-wrap">
								<img src="<?= ICONS ?>contact-tel.png">
							</div>
							<div class="contact-info-wrap">
								<h4 class="contact-info-title">טלפון</h4>
								<a href="tel:<?= $tel; ?>" class="contact-info">
									<?= $tel; ?>
								</a>
							</div>
						</div>
					<?php endif;
					if ($mail) : ?>
						<div class="col-lg-3 col-sm-6 contact-item contact-item-link wow fadeInUp"
							 data-wow-delay="0.4s">
							<div class="contact-icon-wrap">
								<img src="<?= ICONS ?>contact-mail.png">
							</div>
							<div class="contact-info-wrap">
								<h4 class="contact-info-title">מייל</h4>
								<a href="mailto:<?= $mail; ?>" class="contact-info">
									<?= $mail; ?>
								</a>
							</div>
						</div>
					<?php endif;
					if ($address) : ?>
						<div class="col-lg-3 col-sm-6 contact-item-link contact-item wow fadeInUp" data-wow-delay="0.6s">
							<div class="contact-icon-wrap">
								<img src="<?= ICONS ?>contact-geo.png">
							</div>
							<div class="contact-info-wrap">
								<h4 class="contact-info-title">כתובת</h4>
								<a class="contact-info" href="https://www.waze.com/ul?q=<?= $address; ?>">
									<?= $address; ?>
								</a>
							</div>
						</div>
					<?php endif;
					if ($hours) : ?>
						<div class="col-lg-3 col-sm-6 contact-item wow fadeInUp" data-wow-delay="0.8s">
							<div class="contact-icon-wrap">
								<img src="<?= ICONS ?>contact-hours.png">
							</div>
							<div class="contact-info-wrap">
								<h4 class="contact-info-title">שעות פעילות</h4>
								<p class="contact-info">
									<?= $hours; ?>
								</p>
							</div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="col-lg-6">
				<div class="contact-form-wrap">
					<?php if ($fields['contact_title']) : ?>
						<h2 class="form-title">
							<?= $fields['contact_title']; ?>
						</h2>
					<?php endif;
					if ($fields['contact_subtitle']) : ?>
						<h3 class="form-subtitle">
							<?= $fields['contact_subtitle']; ?>
						</h3>
					<?php endif;
					getForm('54'); ?>
				</div>
			</div>
			<?php if ($map) : ?>
				<div class="col-lg-6 col-12">
					<a class="map-image" href="<?= $map['url']; ?>" data-lightbox="map">
						<img src="<?= $map['url']; ?>" alt="map">
					</a>
				</div>
			<?php endif; ?>
		</div>
	</div>
</article>
<?php get_footer(); ?>
