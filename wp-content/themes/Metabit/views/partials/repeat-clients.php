<?php if ($suppliers = opt('h_clients')) :
	$title = opt('clients_title'); ?>
	<section class="clients-block">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto mb-2">
					<h2 class="page-title">
						<?= $title ? $title : esc_html__('בין לקוחותינו', 'leos'); ?>
					</h2>
				</div>
			</div>
		</div>
		<div class="clients-line arrows-slider">
			<div class="container-fluid">
				<div class="row justify-content-center">
					<div class="col-12">
						<div class="partners-slider" dir="rtl">
							<?php foreach ($suppliers as $partner) : ?>
								<div>
									<div class="client-logo">
										<img src="<?= $partner['url']; ?>" alt="customer-logo">
									</div>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>
