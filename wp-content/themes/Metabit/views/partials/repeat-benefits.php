<?php
$benefits = opt('benefits');
$title = opt('benefits_title');
$link = opt('benefits_link');
if ($benefits) : ?>
	<div class="benefits-block" <?php if ($img = opt('benefits_img')) : ?>
		style="background-image: url('<?= $img['url']; ?>')"
	<?php endif; ?>>
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-xl-10 col-12">
					<div class="row justify-content-start">
						<div class="col-xl-7 col-lg-8">
							<?php if ($title) : ?>
								<div class="row">
									<div class="col">
										<h2 class="page-title"><?= $title; ?></h2>
									</div>
								</div>
							<?php endif; ?>
							<div class="row justify-content-center align-items-stretch">
								<?php foreach ($benefits as $x => $why_item) : ?>
									<div class="col-sm-6 col-12 why-item-col wow zoomIn" data-wow-delay="0.<?= $x + 1; ?>s">
										<div class="why-item">
											<div class="why-icon-wrap">
												<?php if ($why_item['icon']) : ?>
													<img src="<?= $why_item['icon']['url']; ?>" alt="benefit-icon">
												<?php endif; ?>
											</div>
											<p class="base-text">
												<?= $why_item['description']; ?>
											</p>
										</div>
									</div>
								<?php endforeach; ?>
							</div>
							<?php if ($link && isset($link['url'])) : ?>
								<div class="row justify-content-center">
									<div class="col-sm-auto col-12">
										<a href="<?= $link['url']; ?>" class="white-link">
											<?= (isset($link['title']) && $link['title']) ? $link['title'] :
												esc_html__('לרכישת פוליסה עכשיו', 'leos'); ?>
										</a>
									</div>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
