<?php if (isset($args['post']) && $args['post']) : $link = get_the_permalink($args['post']); ?>
	<div class="post-card more-card" data-id="<?= $args['post']->ID; ?>">
		<a class="post-item-image" href="<?= $link; ?>">
			<?php if (has_post_thumbnail($args['post'])) : ?>
				<img src="<?= postThumb(); ?>" alt="post-image" class="post-image-card">
			<?php endif; ?>
			<span class="post-item-title-wrap">
				<h3 class="post-item-title"><?= $args['post']->post_title; ?></h3>
				<p class="post-item-text">
					<?= text_preview($args['post']->post_content, 8); ?>
				</p>
			</span>
		</a>
	</div>
<?php endif; ?>
