<?php if (isset($args['product']) && ($args['product'])) : $prod_item = $args['product'];
	$product_all = wc_get_product($prod_item);
	$link = get_permalink($product_all->get_id());
	$id = $product_all->get_id(); ?>
	<div class="card-product">
		<div class="prod-img-wrap" href="<?= $link; ?>">
			<?php if (has_post_thumbnail($prod_item)) : ?>
				<img src="<?= postThumb($prod_item); ?>" class="card-prod-img" alt="icon">
			<?php endif;
			if (get_field('product_days', $prod_item) || get_field('product_days_subtitle', $prod_item)) : ?>
				<div class="d-flex flex-column justify-content-center align-items-center">
					<h4 class="card-days"><?= get_field('product_days', $prod_item); ?></h4>
					<h4 class="card-days-subtitle"><?= get_field('product_days_subtitle', $prod_item); ?></h4>
				</div>
			<?php endif; ?>
		</div>
		<div class="prod-content-wrap">
			<h3 class="product-card-title"><?= $product_all->get_title(); ?></h3>
			<div class="base-output"><?= $product_all->get_short_description(); ?></div>
			<div class="card-prod-price">
				<?php $price = $product_all->get_regular_price(); $price_sale = $product_all->get_sale_price();
				if ($price) : ?>
				<h4 class="base-price">
					<?= get_woocommerce_currency_symbol().$price; ?>
				</h4>
				<?php endif; if ($price_sale) : ?>
				<h4 class="sale-price">
					<?= get_woocommerce_currency_symbol().$price_sale; ?>
				</h4>
				<?php endif; ?>
			</div>
			<a href="<?= $product_all->add_to_cart_url(); ?>" class="base-link">
				<?= esc_html__('לרכישת פוליסה', 'leos')?>
			</a>
		</div>
	</div>
<?php endif; ?>
