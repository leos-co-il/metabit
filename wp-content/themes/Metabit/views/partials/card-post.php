<?php if (isset($args['post']) && $args['post']) : $link = get_the_permalink($args['post']); ?>
	<div class="col-xl-3 col-lg-4 col-sm-6 col-12 post-col">
		<div class="post-card more-card" data-id="<?= $args['post']->ID; ?>">
			<a class="post-item-image" href="<?= $link; ?>">
				<?php if (has_post_thumbnail($args['post'])) : ?>
					<img src="<?= postThumb(); ?>" alt="post-image" class="post-image-card">
				<?php endif; ?>
				<span class="post-item-title-wrap">
					<h3 class="post-item-title"><?= $args['post']->post_title; ?></h3>
				</span>
			</a>
			<div class="post-item-content">
				<p class="post-item-text">
					<?= text_preview($args['post']->post_content, 10); ?>
				</p>
			</div>
			<a href="<?= $link; ?>" class="post-link">
				<?= esc_html__('המשך קריאה> ', 'leos'); ?>
			</a>
		</div>
	</div>
<?php endif; ?>
