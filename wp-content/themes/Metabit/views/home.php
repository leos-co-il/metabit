<?php
/*
Template Name: דף הבית
*/

get_header();
$fields = get_fields();

?>

<?php if ($fields['main_video']) : ?>
	<section class="video-home position-relative">
		<span class="video-home-overlay"></span>
		<video id="video-home" autoplay="autoplay">
			<source src="<?= $fields['main_video']['url']; ?>" type="video/mp4">
		</video>
		<span id="video-button" class="button-home">
			<img src="<?= ICONS ?>play.png" alt="play-video">
			<div class="container">
			<div class="row justify-content-center">
				<div class="col-12">
					<?php if ($fields['main_title']) : ?>
						<h2 class="main-page-title"><?= $fields['main_title']; ?></h2>
					<?php endif;
					if ($fields['main_subtitle']) : ?>
						<h3 class="main-page-subtitle"><?= $fields['main_subtitle']; ?></h3>
					<?php endif; ?>
				</div>
				<?php if ($fields['main_link']) : ?>
					<div class="row justify-content-center">
						<div class="col-auto">
							<a href="<?= $fields['main_link']['url'];?>" class="base-link home-link">
								<?= (isset($fields['main_link']['title']) && $fields['main_link']['title'])
										? $fields['main_link']['title'] : esc_html__('לרכישת פוליסה עכשיו', 'leos');
								?>
							</a>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
		</span>
	</section>
<?php endif;
if ($fields['h_products']) : ?>
	<section class="product-slider-block arrows-slider">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="product-slider" dir="rtl">
						<?php foreach ($fields['h_products'] as $prod) : ?>
							<div class="p-3">
								<?php get_template_part('views/partials/card', 'product',
										[
												'product' => $prod->ID,
										]);
								?>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['h_about_text']) : ?>
	<section class="home-about-block">
		<div class="container">
			<div class="row align-items-center justify-content-between">
				<div class="col-xl-7 col-lg-9 col-12">
					<div class="base-output">
						<?= $fields['h_about_text']; ?>
					</div>
					<?php if ($fields['h_about_link']) : ?>
						<div class="row justify-content-end">
							<div class="col-auto">
								<a href="<?= $fields['h_about_link']['url'];?>" class="base-link">
									<?= (isset($fields['h_about_link']['title']) && $fields['h_about_link']['title'])
											? $fields['h_about_link']['title'] : esc_html__('קרא עוד עלינו ', 'leos');
									?>
								</a>
							</div>
						</div>
					<?php endif; ?>
				</div>
				<?php if ($logo = opt('logo_about')) : ?>
					<div class="col-lg-3">
						<a href="<?= home_url(); ?>" class="logo-about">
							<img src="<?= $logo['url'] ?>" alt="logo">
						</a>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</section>
<?php endif;
get_template_part('views/partials/repeat', 'benefits');
get_template_part('views/partials/repeat', 'clients');
if ($fields['h_posts']) : ?>
	<section class="home-blog-block arrows-slider arrows-slider-base">
		<div class="put-arrows-here"></div>
		<div class="container position-relative">
			<div class="blog-container"></div>
			<div class="row flex-lg-nowrap">
				<div class="col-lg-5 col-12">
					<div class="inside-text-wrapper">
						<div class="base-output">
							<?= $fields['h_posts_text']; ?>
						</div>
						<?php if (['h_about_linkh_posts_link']) : ?>
							<div class="row justify-content-end w-100">
								<div class="col-auto">
									<a href="<?= $fields['h_posts_link']['url'];?>" class="base-link">
										<?= (isset($fields['h_posts_link']['title']) && $fields['h_posts_link']['title'])
												? $fields['h_posts_link']['title'] : esc_html__('לכל הכתבות', 'leos');
										?>
									</a>
								</div>
							</div>
						<?php endif; ?>
					</div>
				</div>
				<div class="col-xl-11 col-lg-9 col-12">
					<div class="col-post-slider">
						<div class="posts-slider" dir="rtl">
							<?php foreach ($fields['h_posts'] as $post) : ?>
								<div class="p-3">
									<?php get_template_part('views/partials/card', 'post_slider',
											[
													'post' => $post,
											]);
									?>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
	]);
}
get_footer(); ?>
