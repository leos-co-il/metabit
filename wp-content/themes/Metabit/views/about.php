<?php
/*
Template Name: אודות
*/
the_post();
get_header();
$fields = get_fields();
?>
<article class="page-body">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container pb-5">
		<div class="row justify-content-between align-items-center">
			<div class="col-xl-8 col-lg-9 col-12">
				<div class="base-output">
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</div>
			</div>
			<?php if ($logo = opt('logo_about')) : ?>
				<div class="col-lg-3">
					<a class="logo logo-about" href="<?= $logo; ?>">
						<img src="<?= $logo['url']; ?>" alt="logo">
					</a>
				</div>
			<?php endif; ?>
		</div>
	</div>
	<?php if ($fields['about_team']) : ?>
		<div class="team-block arrows-slider">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="team-slider" dir="rtl">
							<?php foreach ($fields['about_team'] as $item) :?>
								<div class="p-2">
									<div class="post-card">
										<div class="post-item-image">
											<?php if (isset($item['img']) && $item['img']) : ?>
												<img src="<?= $item['img']['url']; ?>" alt="post-image" class="post-image-card">
											<?php endif; ?>
											<span class="post-item-title-wrap">
											<h3 class="post-item-title"><?= $item['name']; ?></h3>
											<h3 class="post-item-title">
												<?= esc_html__('תפקיד - ', 'leos').$item['position']; ?>
											</h3>
										</span>
										</div>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
</article>
<?php get_template_part('views/partials/repeat', 'benefits');
get_template_part('views/partials/repeat', 'clients');
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
	]);
}
get_footer(); ?>
