<?php
/*
Template Name: טופס הצטרפות
*/
the_post();
get_header();
$fields = get_fields();
?>
<article class="page-body faq">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-10 col-sm-11 col-12">
				<h1 class="page-title text-center mb-4"><?php the_title(); ?></h1>
				<div class="base-output text-center">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="form-basic-block">
					<form method="post">
						<div class="row">
							<div class="col-sm-6 input-col">
								<label for="select-type"><?= esc_html__('סוג פוליסה:', 'leos'); ?></label>
								<select id="select-type" name="type">
									<option disabled selected><?= esc_html__('ביטוח שנתי', 'leos'); ?></option>
									<option><?= esc_html__('לורם איפסום 1', 'leos'); ?></option>
									<option><?= esc_html__('לורם איפסום 2', 'leos'); ?></option>
								</select>
							</div>
							<div class="col-sm-6 input-col">
								<label for="select-club"><?= esc_html__('מועדון צלילה:', 'leos'); ?></label>
								<select id="select-club" name="club">
									<option disabled selected><?= esc_html__('בחר', 'leos'); ?></option>
									<option><?= esc_html__('לורם איפסום 11', 'leos'); ?></option>
									<option><?= esc_html__('לורם איפסום 22', 'leos'); ?></option>
								</select>
							</div>
							<div class="col-lg-4 col-sm-6 input-col">
								<label for="doc-number"><?= esc_html__('ת.ז:', 'leos'); ?></label>
								<input id="doc-number" name="passport" type="text">
							</div>
							<div class="col-lg-4 col-sm-6 input-col">
								<label for="first-name-en"><?= esc_html__('שם פרטי באנגלית:', 'leos'); ?></label>
								<input id="first-name-en" name="name-first-en" type="text">
							</div>
							<div class="col-lg-4 col-sm-6 input-col">
								<label for="last-name-en"><?= esc_html__('שם משפחה באנגלית:', 'leos'); ?></label>
								<input id="last-name-en" name="name-last-en" type="text">
							</div>
							<div class="col-lg-4 col-sm-6 input-col">
								<label for="phone-number"><?= esc_html__('טלפון:', 'leos'); ?></label>
								<input id="phone-number" name="phone" type="tel">
							</div>
							<div class="col-lg-4 col-sm-6 input-col">
								<label for="first-name"><?= esc_html__('שם פרטי:', 'leos'); ?></label>
								<input id="first-name" name="name-first" type="text">
							</div>
							<div class="col-lg-4 col-sm-6 input-col">
								<label for="last-name"><?= esc_html__('שם משפחה:', 'leos'); ?></label>
								<input id="last-name" name="name-last" type="text">
							</div>
							<div class="col-lg-4 col-sm-6 input-col">
								<label for="phone-number-mob"><?= esc_html__('טלפון סלולרי:', 'leos'); ?></label>
								<input id="phone-number-mob" name="phone-mob" type="tel">
							</div>
							<div class="col-lg-4 col-sm-6 input-col">
								<label for="address-name"><?= esc_html__('כתובת:', 'leos'); ?></label>
								<input id="address-name" name="address" type="text">
							</div>
							<div class="col-lg-4 col-sm-6 input-col">
								<label for="city-name"><?= esc_html__('עיר:', 'leos'); ?></label>
								<input id="city-name" name="city" type="text">
							</div>
							<div class="col-lg-4 col-sm-6 input-col">
								<label for="position-name"><?= esc_html__('מיקוד:', 'leos'); ?></label>
								<input id="position-name" name="position" type="text">
							</div>
							<div class="col-lg-4 col-sm-6 input-col">
								<label for="date-of-birth"><?= esc_html__('תאריך לידה:', 'leos'); ?></label>
								<input id="date-of-birth" name="date" type="date">
							</div>
							<div class="col-lg-4 col-sm-6 input-col">
								<label for="diver-rank"><?= esc_html__('דרגת צולל:', 'leos'); ?></label>
								<select id="diver-rank" name="level">
									<option disabled selected></option>
									<option><?= esc_html__('לורם איפסום 1', 'leos'); ?></option>
									<option><?= esc_html__('לורם איפסום 2', 'leos'); ?></option>
								</select>
							</div>
							<div class="col-sm-6 input-col">
								<label for="email-address"><?= esc_html__('כתובת מייל:', 'leos'); ?></label>
								<input id="email-address" name="email" type="email">
							</div>
							<div class="col-sm-6 input-col">
								<label for="rang-name"><?= esc_html__('תאריך לידה:', 'leos'); ?></label>
								<input id="rang-name" name="rang" type="text" placeholder="<?= esc_html__('יורשים חוקיים', 'leos');?>">
							</div>
						</div>
						<div class="row justify-content-center">
							<div class="col-auto col-checkbox">
								<input type="checkbox" name="acceptance" id="acceptance-input" required />
								<label for="acceptance-input"><?= esc_html__('אני מעוניין לקבל עדכונים בנושא ביטוח צלילה ', 'leos'); ?></label>
							</div>
						</div>
						<div class="row justify-content-center">
							<div class="col-lg-auto col-md-6 col-12">
								<a href="#" class="submit-link"><?= esc_html__('הוסף צולל נוסף', 'leos'); ?></a>
							</div>
							<div class="col-lg-auto col-md-6 col-12">
								<a href="#" class="submit-link submit-link-arrow"><?= esc_html__('שלב הבא', 'leos'); ?></a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</article>
<?php get_footer(); ?>
