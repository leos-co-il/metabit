<?php
/*
Template Name: שיחזור תעודת צולל
*/
the_post();
get_header();
$fields = get_fields();
?>
<article class="page-body">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container pb-5">
		<div class="row justify-content-between align-items-center">
			<div class="col-12">
				<div class="base-output text-center">
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</div>
			</div>
		</div>
		<?php if ($fields['recovery_item']) : ?>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($fields['recovery_item'] as $x => $item) : ?>
					<div class="col-lg-4 col-sm-6 col-12 logo-diver-col">
						<?php if ($item['logo'] && isset($item['logo']['url'])) : ?>
							<img src="<?= $item['logo']['url']; ?>" alt="logo" class="logo-item">
						<?php endif;
						if ($item['url']) : ?>
							<a href="<?= $item['url']['url']; ?>" class="post-link">
								<?= (isset($item['url']['title']) && $item['url']['title'])
									? $item['url']['title'] : '';
								?>
							</a>
						<?php endif; ?>
					</div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
</article>
<?php if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
get_footer(); ?>
