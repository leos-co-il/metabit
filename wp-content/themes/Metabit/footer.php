<?php

$tel = opt('tel');
$mail = opt('mail');
$fax = opt('fax');
$address = opt('address');
$open_hours = opt('open_hours');
$logo = opt('logo_white');
$map = opt('map_image');

?>

<footer>
	<div class="footer-main" <?php if ($back = opt('foo_back')) : ?>
		style="background-image: url('<?= $back['url']; ?>')"
	<?php endif; ?>>
		<a id="go-top">
			<span class="to-top-wrapper">
				<img src="<?= ICONS ?>go-top.png" alt="go-top-icon">
			</span>
			<span class="to-top-text">
				<?= esc_html__('חזרה למעלה', 'leos'); ?>
			</span>
		</a>
		<div class="foo-form-wrap">
			<div class="container">
				<div class="row justify-content-center">
					<?php if ($logo) : ?>
						<div class="col-md-3 col-sm-5">
							<a href="/" class="logo">
								<img src="<?= $logo['url'] ?>" alt="logo-rdi">
							</a>
						</div>
					<?php endif; ?>
					<div class="col-xl-11 col-12">
						<?php if ($f_title = opt('foo_form_title')) : ?>
							<h2 class="foo-form-title"><?= $f_title; ?></h2>
						<?php endif;
						getForm('56'); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="container position-relative foo-container">
			<div class="row justify-content-center">
				<div class="col-xl-10 col-12">
					<div class="row justify-content-between">
						<div class="col-xl-3 col-sm-6 col-12 foo-menu main-footer-menu">
							<h3 class="foo-title">
								<?= esc_html__('תפריט ראשי //', 'leos') ?>
							</h3>
							<div class="menu-border-top">
								<?php getMenu('footer-menu', '1', 'hop-hey'); ?>
							</div>
						</div>
						<div class="col-lg col-12 foo-menu links-footer-menu">
							<h3 class="foo-title">
								<?php $menu_title = opt('foo_menu_title');
								echo $menu_title ? $menu_title : esc_html__('מאמרים רלוונטיים //', 'leos');?>
							</h3>
							<div class="menu-border-top">
								<?php getMenu('footer-links-menu', '1', 'hop-hey three-columns'); ?>
							</div>
						</div>
						<div class="col-xl-3 col-sm-6 col-12 foo-menu contacts-footer-menu">
							<h3 class="foo-title">
								<?= esc_html__('פרטי התקשרות // ', 'leos'); ?>
							</h3>
							<div class="menu-border-top contact-menu-foo">
								<ul class="contact-list d-flex flex-column">
									<?php if ($tel) : ?>
										<li>
											<a href="tel:<?= $tel; ?>" class="contact-info-footer">
												<?= svg_simple(ICONS.'foo-tel.svg'); ?>
												<span><?= $tel; ?></span>
											</a>
										</li>
									<?php endif;
									if ($address) : ?>
										<li>
											<a href="https://waze.com/ul?q=<?= $address; ?>"
											   class="contact-info-footer" target="_blank">
												<?= svg_simple(ICONS.'foo-geo.svg'); ?>
												<span><?= $address; ?></span>
											</a>
										</li>
									<?php endif;
									if ($mail) : ?>
										<li>
											<a href="mailto:<?= $mail; ?>" class="contact-info-footer">
												<?= svg_simple(ICONS.'foo-email.svg'); ?>
												<span><?= $mail; ?></span>
											</a>
										</li>
									<?php endif; ?>
								</ul>
								<div class="foo-socials">
									<div class="pop-trigger">
										<img src="<?= ICONS ?>foo-mail.png" alt="contact-usl">
									</div>
									<?php if ($whatsapp = opt('whatsapp')) : ?>
										<a href="https://api.whatsapp.com/send?phone=<?= $whatsapp; ?>" target="_blank" class="social-link">
											<img src="<?= ICONS ?>foo-whatsapp.png" alt="whatsapp-call">
										</a>
									<?php endif;
									if ($facebook = opt('facebook')) : ?>
										<a href="<?= $facebook; ?>" target="_blank" class="social-link">
											<img src="<?= ICONS ?>foo-facebook.png" alt="facebook-call">
										</a>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="leos">
		<a href="http://www.leos.co.il/" title="לאוס מדיה ואינטראקטיב">
			<img src="<?= IMG . 'leos_logo.png' ?>"
				 alt="" title="קידום אתרים עם לאוס מדיה ואינטראקטיב | חברה לקידום אתרים ובניית אתרים" />
			<span></span>
		</a>
	</div>
</footer>
<?php wp_footer(); ?>

<?php

if(ENV === 'dev'):
	require_once THEMEPATH . "/inc/debug.php"
	?>
	<script>

		function _fetchHeader($_el){
			let res = {
				'count' : 0,
				'content' : ''
			} ;
			$($_el).each(function () {
				res.count++;
				res.content += ' [' + $(this).text() + '] ';
			});
			return 'Count: ' + res.count + '. Text: ' + res.content;
		}

		function _fetchMeta($_meta){
			return $('meta[name='+$_meta+']').attr("content");
		}




		phpdebugbar.addDataSet({
			"SEO Local": {
				'H1' : _fetchHeader('h1'),
				'H2' : _fetchHeader('h2'),
				'H3' : _fetchHeader('h3'),
				'Meta Title' : _fetchMeta('title'),
				'Meta Description' : _fetchMeta('description'),
				'Meta Keywords' : _fetchMeta('keywords'),
			}
		});
	</script>

<?php endif; ?>

</body>
</html>
